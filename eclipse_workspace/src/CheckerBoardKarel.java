import stanford.karel.SuperKarel;

public class CheckerBoardKarel extends SuperKarel {
    
    public void run() {
        putBeeper();
        move();
        
        while (frontIsClear()) {
            move();
            putBeeper();
            if (frontIsClear()) {
                move();
            }
            
            if (isEndOfLineAndNotEndOfWorld()) {
                turnAction();
                if (beepersPresent()) {
                    move();
                    turnAction();
                } else {
                    move();
                    putBeeper();
                    turnAction();
                    move();
                }
            }
        }
        turnRight();
        turnRight();
    }
	
	private boolean isEndOfLineAndNotEndOfWorld() {
		return frontIsBlocked() && ((leftIsClear() && facingEast()) || (rightIsClear() && facingWest()));
	}
	
	private void turnAction() {
        if (facingEast() || rightIsBlocked())
            turnLeft();
        else
            turnRight();
    }
}
