import stanford.karel.*;

public class AdditionKarel extends SuperKarel {
  
  private int counter = 0;
  private int transfer = 0;
  
	public void run() {
	  
	  moveToWall();
	  turnLeft();
	  pickUp();
	  moveToWall();
	  putNumber();
	  
	  while(frontIsClear()) {
	    pickUp();
	    moveToWall();
	    putNumber();
	  } 
	}
	
	private void pickUp() {
	  
	  while(!beepersPresent()) {
	    move();
	  }
	  
	  counter = transfer;
	  transfer = 0;
	  
	  while(beepersPresent() || frontIsClear()) {
	    
	    while(beepersPresent()) {
	      
	        pickBeeper();
	        counter++;
	      }
	    move();
	  } 
	  turnAround();
	  
	}
	
	private void putNumber() {
	  
	  transfer = counter / 10;
	  counter = counter % 10;
	  
	  for(int i = 0; i < counter; i++) {
	    putBeeper();
	  }
	  
	  turnRight();
	  if(frontIsBlocked()) {
	    return;
	  }
      move();
      turnRight();
	}
	
	
	private void moveToWall() {
	  
	  while(frontIsClear())
	    move();
	}
	
	@Override
	public void move() {
	  if(frontIsClear())
	    super.move();
	}
}

