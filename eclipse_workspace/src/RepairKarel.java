import stanford.karel.SuperKarel;

public class RepairKarel extends SuperKarel {
    
    public void run() {
        turnLeft();
        while (frontIsClear()) {
            placeBeepersInYAxis();
            goToNextLine();
        }
    }
    
    private void placeBeepersInYAxis() {
        do {
            if (noBeepersPresent())
                putBeeper();
        } while (optionalMove());
    }
    
    private boolean optionalMove() {
        if (frontIsBlocked())
            return false;
        move();
        return true;
    }
    
    private void goToNextLine() {
        Runnable turnAction = getTurnAction();
        turnAction.run();
        
        if (frontIsClear()) {
            move();
            move();
            move();
            move();
            turnAction.run();
        }
    }
    
    private Runnable getTurnAction() {
        if (facingNorth())
            return this::turnRight;
        else
            return this::turnLeft;
    }
    
}
