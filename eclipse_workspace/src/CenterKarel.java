import stanford.karel.SuperKarel;

public class CenterKarel extends SuperKarel {
    
    public void run() {
        putBeeper();
        
        if (!frontIsBlocked())
            findCenter();
        turnToOtherSide();
        if (!frontIsBlocked())
            findCenter();
    }
    
    private void findCenter() {
        while (moveToNextBeeper()) {
            putBeeper();
        }
    }
    
    private boolean moveToNextBeeper() {
        do {
            move();
        } while (frontIsClear() && noBeepersPresent());
        
        turnAround();
        if (beepersPresent()) {
            pickBeeper();
            move();
            return noBeepersPresent();
        }
        return true;
    }
    
    private void turnToOtherSide() {
        if (rightIsBlocked())
            turnLeft();
        else
            turnRight();
    }
}
