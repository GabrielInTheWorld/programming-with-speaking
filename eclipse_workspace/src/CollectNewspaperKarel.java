import stanford.karel.*;

public class CollectNewspaperKarel extends SuperKarel {

  public void run() {
    turnRight();
    move();
    turnLeft();
    move();
    move();
    move();
    pickBeeper();
    turnRight();
    turnRight();
    move();
    move();
    move();
    turnRight();
    move();
    turnRight();
  }
}
