/*
 * File: BlankKarel.java --------------------- This class is a blank one that you can change to whatever you want!
 */

import stanford.karel.*;

public class CleaningKarel extends SuperKarel {

  public void run() {
    do {
      do {
        cleanUpAllBeepersOfThisPosition();
      } while (optionalMove());
    } while (nextLine());
  }
  
  private void cleanUpAllBeepersOfThisPosition() {
    while (beepersPresent())
      pickBeeper();
  }
  
  private boolean optionalMove() {
    if (frontIsBlocked())
      return false;
    move();
    return true;
  }
  
  private boolean nextLine() {
    turnAction();
    if (frontIsBlocked())
      return false;
    move();
    turnAction();
    return true;
  }
  
  private void turnAction() {
    if (facingWest() || leftIsBlocked())
      turnRight();
    else
      turnLeft();
  }
  
}

