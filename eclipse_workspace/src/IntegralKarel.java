import stanford.karel.*;

public class IntegralKarel extends SuperKarel {

  public void run() {
    collectYValuesToXAxis();
    collectYValuesToOne();
  }
  
  private void collectYValuesToXAxis() {
    do {
      turnLeft();
      placeBeepersInYAxis();
      collectBeepersToXAxis();
      goToNextXValue();
    } while (frontIsClear() || noBeepersPresent());
  }
  
  private void placeBeepersInYAxis() {
    while (frontIsClear()) {
      putBeeper();
      move();
    }
    putBeeper();
  }
  
  private void collectBeepersToXAxis() {
    do {
      if (beepersPresent()) {
        bringBeeperToOtherWall();
      }
      move();
    } while (frontIsClear());
    
    turnAround();
    returnToWall();
  }
  
  private void goToNextXValue() {
    turnLeft();
    if (frontIsClear())
      move();
  }
  
  private void collectYValuesToOne() {
    do {
      if (beepersPresent()) {
        bringBeeperToOtherWall();
      }
      move();
    } while (frontIsClear() || beepersPresent());
  }
  
  private void bringBeeperToOtherWall() {
    turnAround();
    pickBeeper();
    returnToWall();
    putBeeper();
    turnAround();
  }
  
  private void returnToWall() {

    while (frontIsClear()) {
      move();
    }
  }
}
