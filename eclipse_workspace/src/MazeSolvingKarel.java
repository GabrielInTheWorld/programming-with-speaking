/*
 * MazeSolvingKarel.java This program implements an algorithm for escaping from a maze. The "exit" of the maze is
 * represented by a beeper.
 */
import stanford.karel.*;

public class MazeSolvingKarel extends SuperKarel {

  public void run() {

    while (noBeepersPresent()) {
      if (leftIsClear())
        turnLeft();
      else if (frontIsBlocked() && rightIsClear())
        turnRight();
      else if (frontIsBlocked())
        turnAround();
      move();
    }
  }
}
