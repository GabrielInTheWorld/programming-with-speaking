# The following line imports all the dragonfly stuff we'll be using -- obviously you must have Dragonfly installed
from dragonfly import *
from castervoice.lib.merge.state.short import R


# Now we make our MappingRule object with only two commands
class MyRule(MappingRule):
	mapping = {
		"laws facing":
			R(Text("facing")),
		"laws karel":
			R(Text("karel")),
		"sing karel":
			R(Text("Karel")),
		"laws beepers":
			R(Text("beepers")),
		"sing beeper":
			R(Text("Beeper")),
		"close this window":
			R(Key("a-f4")),
		"run karel":
			R(Key("f9")),
		"run last karel":
			R(Key("f10")),
		"completion":
			R(Key("c-space")),
		"open type":
			R(Key("cs-t")),
		"outline":
			R(Key("c-o")),
		"eclipse command":
			R(Key("c-3")),
		"call function":
			R(Text("();")),
		"call function shock":
			R(Text("();") + Key("enter")),
		"new private method":
			R(Text("private_method") + Pause("10") + Key("c-space") + Key("enter")),
		"new method":
			R(Text("private_method") + Pause("10") + Key("c-space") + Key("enter")),
		"for loop":
			R(Text("for") + Pause("10") + Key("c-space") + Key("enter")),
		"do loop":
			R(Text("do") + Pause("10") + Key("c-space") + Key("enter")),
		"while loop":
			R(Text("while") + Pause("10") + Key("c-space") + Key("down,down,enter")),
		"(if | iffae)":
			R(Text("if") + Pause("10") + Key("c-space") + Key("enter")),
		"(else | shells)":
			R(Text("else") + Pause("10") + Key("c-space") + Key("enter")),
		"(else if | shells iffae)":
			R(Text("elseif") + Pause("10") + Key("c-space") + Key("enter")),
		"new line":
			R(Key("end,enter")),
		"flush line":
			R(Key("end") + Key("s-home") + Key("delete")),
		"delete line":
			R(Key("c-d")),
		"lodge and":
			R(Text("&&")),
		"lodge not":
			R(Text("!")),
		"lodge or":
			R(Text("||")),
		"laws boolean":
			R(Text("boolean")),

	}

# Now let's create our Grammar object and add an instance of our rule to it:
grammar = Grammar("my_new_grammar")
grammar.add_rule(MyRule())
grammar.load()