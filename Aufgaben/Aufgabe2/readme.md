# Anleitung für Aufgabe Basics #

Nun kannst du versuchen, Methoden selbstständig zu programmieren, da du bereits die Grundlagen kennst.
Dazu wird zunächst ein Ergebnis gezeigt, wie eine Methode am Ende aussehen soll. 
Entweder programmierst du eigenständig eine Methode nach oder gehst die Anweisungen durch, die unter dem Ergebnis stehen.

### I: Wechseln zur Klasse "CleaningKarel"
1. *open type* &rarr; Das Dialog Fenster "Open Type" wird zur Auswahl einer Klasse geöffnet.
2. *laws cleaning* &rarr; Gibt "cleaning" ein.
3. *shock* &rarr; Wählt den ersten Eintrag "CleaningKarel" aus.

![](./Bilder/eclipse_open_type.png)

```java
import stanford.karel.*;

public class CleaningKarel extends SuperKarel {

  public void run() {
	  
  }
  
}
```

### II: Neue Methode "turnAction" erstellen
```java
  private void turnAction() {
    if (facingWest() || leftIsBlocked()) {
      turnRight();
    }
    else {
      turnLeft();
    }
  }
```

1. *dunce five* &rarr; Der Cursor geht fünf Zeile tiefer.
1. *new method* &rarr; Das Live-Template für eine neue private Methode wird aufgerufen.
1. *tabby* &rarr; Return-Type bleibt "void".
1. *laws turn* &rarr; Gibt "turn" ein.
1. *sing Action* &rarr; Gibt "Action" ein.
1. *shock* &rarr; Methodenbearbeitung beenden.
1. *if* &rarr; IF-Bedingung wird geschrieben.
1. *laws facing* &rarr; Gibt "facing" ein.
1. *sing west* &rarr; Gibt "West" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen und die Klammern werden automatisch gesetzt.
1. *shock* &rarr; Auwählen der Methode "facingWest()"
1. *ace* &rarr; Schreibt ein Leerzeichen.
1. *lodge or* &rarr; Schreibt die Oder-Verküpfung ||.
1. *ace* &rarr; Schreibt ein Leerzeichen.
1. *laws left* &rarr; Gibt "left" ein.
1. *sing Is* &rarr; Gibt "Is" ein.
1. *sing Blocked* &rarr; Gibt "Blocked" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen und die Klammern werden automatisch gesetzt.
1. *shock* &rarr; If-Bearbeitung beenden.
1. *laws turn* &rarr; Gibt "turn" ein.
1. *sing Right* &rarr; Gibt "Right" ein. 
1. *call function* &rarr; Schreibt "();"
1. *dunce five* &rarr; Der Cursor geht eine Zeile tiefer.
1. *ace* &rarr; Schreibt ein Leerzeichen.
1. *else* &rarr; else-Block wird geschrieben.
1. *laws turn* &rarr; Gibt "turn" ein.
1. *sing Left* &rarr; Gibt "Left" ein.
1. *call function* &rarr; Schreibt "();", **ohne** neue Zeile.


### III: Neue Methode "nextLine" erstellen
```java
  private boolean nextLine() {
    turnAction();
    if (frontIsBlocked())  {
      return false;
    }
    move();
    turnAction();
    return true;
  }
```

1. *dunce two* &rarr; Der Cursor geht zwei Zeile tiefer.
1. *shock* &rarr; neue Zeile
1. *shock* &rarr; neue Zeile
1. *new method* &rarr; Das Live-Template für eine neue private Methode wird aufgerufen.
1. *laws boolean* &rarr; Gibt "boolean" ein.
1. *tabby* &rarr; Springt zum Methodennamen
1. *laws next* &rarr; Gibt "next" ein.
1. *sing Line* &rarr; Gibt "Line" ein.
1. *shock* &rarr; Methodenbearbeitung beenden.
1. *laws turn* &rarr; Gibt "turn" ein.
1. *sing Action* &rarr; Gibt "Action" ein.
1. *call function shock* &rarr; Schreibt "();" und eine neue Zeile. 
1. *if* &rarr; IF-Bedingung wird geschrieben.
1. *laws front* &rarr; Gibt "front" ein.
1. *sing Is* &rarr; Gibt "Is" ein.
1. *sing Blocked* &rarr; Gibt "Blocked" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen und die Klammern werden automatisch gesetzt.
1. *shock* &rarr; if-Bearbeitung beenden.
1. *laws return* &rarr; Gibt "return" ein.
1. *laws false* &rarr; Gibt "false" ein.
1. *semper* &rarr Schreibt ein Semikolon.
1. *dunce* &rarr; Der Cursor geht eine Zeile tiefer.
1. *shock* &rarr; neue Zeile
1. *laws move* &rarr; Gibt "move" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen und die Klammern werden automatisch gesetzt.
1. *shock* &rarr; neue Zeile
1. *laws turn* &rarr; Gibt "turn" ein.
1. *sing Action* &rarr; Gibt "Action" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen und die Klammern werden automatisch gesetzt.
1. *shock* &rarr; Auswählen der Funktion "turnAction()".
1. *laws return* &rarr; Gibt "return" ein.
1. *ace* &rarr; Schreibt ein Leerzeichen.
1. *laws true* &rarr; Gibt "true" ein.
1. *semper* &rarr Schreibt ein Semikolon.

### III: Neue Methode "optionalMove" erstellen
```java
  private boolean optionalMove() {
    if (frontIsBlocked()) {
      return false;
    }
    move();
    return true;
  }
```

1. *dunce* &rarr; Der Cursor geht eine Zeile tiefer.
1. *shock* &rarr; neue Zeile
1. *shock* &rarr; neue Zeile
1. *new method* &rarr; Das Live-Template für eine neue private Methode wird aufgerufen.
1. *laws boolean* &rarr; Gibt "boolean" ein.
1. *tabby* &rarr; Springt zum Methodennamen
1. *laws optional* &rarr; Gibt "optional" ein.
1. *sing Move* &rarr; Gibt "Move" ein.
1. *shock* &rarr; Methodenbearbeitung beenden.
1. *if* &rarr; IF-Bedingung wird geschrieben.
1. *laws front* &rarr; Gibt "front" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen.
1. *shock* &rarr; Wählt den ersten Eintrag "frontIsBlocked" aus.
1. *shock* &rarr; if-Bearbeitung beenden.
1. *laws return* &rarr; Gibt "return" ein.
1. *ace* &rarr; Schreibt ein Leerzeichen.
1. *laws false* &rarr; Gibt "false" ein.
1. *semper* &rarr Schreibt ein Semikolon.
1. *dunce* &rarr; Der Cursor geht eine Zeile tiefer.
1. *shock* &rarr; neue Zeile
1. *laws move* &rarr; Gibt "move" ein.
1. *call function shock* &rarr; Schreibt "();" und eine neue Zeile.
1. *laws return* &rarr; Gibt "return" ein.
1. *ace* &rarr; Schreibt ein Leerzeichen.
1. *laws true* &rarr; Gibt "true" ein.
1. *semper* &rarr Schreibt ein Semikolon.

### IV: Methode "run" ausimplementieren ###
```java 
  public void run() {
    do {
      do {
    	  while (beepersPresent())
    	      pickBeeper();
      } while (optionalMove());
    } while (nextLine());
  }
```

1. *outline* &rarr; Öffnet das Quick Outline Fenster
1. *tabby* &rarr; Geht ins Feld mit den Methoden.
1. *dunce* &rarr; Der Cursor geht eine Zeile tiefer.
1. *enter* &rarr; Wählt den Eintrag "run" aus.
1. *dunce* &rarr; Der Cursor geht eine Zeile tiefer.
1. *do loop* &rarr; Do-While Schleife wird geschrieben.
1. *laws next* &rarr; Gibt "next" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen.
1. *shock* &rarr; Wählt den ersten Eintrag "nextLine()" aus.
1. *shock* &rarr; Beendet Do-Loop Bedingung.
1. *do loop* &rarr; Do-While Schleife wird geschrieben.
1. *laws optional* &rarr; Gibt "optional" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen.
1. *shock* &rarr; Wählt den ersten Eintrag "optionalMove()" aus.
1. *shock* &rarr; Beendet Do-Loop Bedingung.
1. *while loop* &rarr; Schreibt eine While Loop.
1. *laws beepers* &rarr; Gibt "beepers" ein.
1. *sing Present* &rarr; Gibt "Present" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen und die Klammern werden automatisch gesetzt.
1. *shock* &rarr; Wählt den ersten Eintrag "beepersPresent()" aus.
1. *shock* &rarr; Beendet While-Loop Bedingung.
1. *laws pick* &rarr; Gibt "pick" ein.
1. *completion* &rarr; Autocompletion wird aufgerufen.
1. *shock* &rarr; Wählt den ersten Eintrag "pickBeeper()" aus.


### IV: Programm starten ###
1. *save* &rarr; Speichert die Datei.
1. *run last karel* &rarr; Startet das Programm.
1. *tabby* &rarr; Wählt in dem Button "Start Program" aus.
1. *shock* &rarr; Drückt auf den Button.


## Fülle nun die erste Seite des Fragebogens aus

https://docs.google.com/forms/d/e/1FAIpQLSdFterRGgbLz5jBds68-nk1w4Y596cziqQ7MrTy8kc3p5lCVw/viewform