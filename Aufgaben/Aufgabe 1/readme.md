# Anleitung für Aufgabe Basics #

## Teil 1: Die erste Zeitung aufheben ##

Öffne Eclipse manuell mit dem Karel-Projekt.

### I: Neue Klasse Basics erstellen ###
1. *eclipse command* &rarr; Eine Befehlssuche öffnet sich.
2. *laws new class* &rarr; Gibt "new class" ein.
1. Bei der ersten Ausführung muss ggf. noch mit den Tasten nach unten getippt werden.
3. *shock* &rarr; Wählt den markierten Eintrag aus.
4. *sing basics* &rarr; Gibt als Klassennamen "Basics" ein.
5. *shock* &rarr; Bestätigt das Menü und erstellt eine neue Klasse.

![](./Bilder/eclipse_command_new_class.png)

![](./Bilder/new_class_wizzard.png)

```java 
package karel;

public class Basics {

}
```

### II: Import-Zeile ###
1. *ross wally* &rarr; Der Cursor spring ans Ende der ersten Zeile
2. *shock* &rarr; Eine neue Zeile wird eingefügt.
3. *laws import* &rarr; Schreibt "import". 
4. *ace* &rarr; Schreibt ein Leerzeichen.
5. *laws stanford* &rarr; Schreibt "stanford".
6. *period* &rarr; Schreibt einen Punkt.
7. *laws karel* &rarr; Schreibt "karel"; die Autovervollständigung macht einen Vorschlag.
8. *shock* &rarr; Wählt den Vorschlag der Autovervollständigung aus.

```java 
package karel;  
import stanford.karel.*;

public class Basics {

}
```

### III: Vererbung eingeben ###
1. *dunce two* &rarr; Cursor spring in die Zeile der Klasse Basics.
1. *lease* &rarr; Cursor spring vor die geschweifte Klammer.
1. *laws extends* &rarr; Schreibt "extends".
1. *ace* &rarr; Schreibt ein Leerzeichen.
1. *sing super* &rarr; Schreibt "Super".
1. *sing karel* &rarr; Schreibt "Karel".
1. *ace* &rarr; Schreibt ein Leerzeichen.

```java 
public class Basics extends SuperKarel {

}
```

### IV: Implementiere die Funktion run() ###
1. *eclipse command* &rarr; Öffnet die Befehlssuche.
1. *laws override* &rarr; Schreibt "override".
1. *shock* &rarr; Wählt ersten Eintrag aus: "Override/Implement Methods"
1. *dunce eleven* &rarr; Makiert den 11. Eintrag, also "run()".
1. *ace* &rarr; Wählt den Eintrag aus.
1. *shock* &rarr; Bestätigt das Menü und implementiert die Funktion.
1. *Dunce four* &rarr; Geht in die erste Zeile der Methode `run()`
1. *delete line* &rarr; Löscht die aktuelle Zeile
1. *flush line* &rarr; Löscht die aktuelle Zeile, außer den Tab.

![](./Bilder/eclipse_command_override.png)

![](./Bilder/override_wizzard.png)

```java 
public class Basics extends SuperKarel {  

    @Override  
    public void run() {  
        
    }  

}  
```

### V: Drei Schritte vor ###
```java 
    @Override  
    public void run() {  
        move();  
        move();  
        move();  
        
    }
```

Drei Mal:

1. *laws move* &rarr; Schreibt "move".
1. *call function shock* &rarr; Schreibt "();" und eine neue Zeile.  

Alternative:

1. *laws move* &rarr; Schreibt "move".
1. *completion* &rarr; Autocompletion wird aufgerufen.
1. *shock* &rarr; Wählt den ersten Eintrag "move()" aus.
1. *shock* &rarr; Schreibt eine neue Zeile.

### VI: Drehung nach links ###
```java 
    @Override  
    public void run() {  
        move();  
        move();  
        move();  
        turnLeft();  
          
    }
```

1. *laws turn* &rarr; Schreibt "turn".
1. *sing left* &rarr; Schreibt "Left".
1. *call function shock* &rarr; Schreibt "();" und eine neue Zeile.

Alternative:

1. *laws turn* &rarr; Schreibt "turn".
1. *completion* &rarr; Autocompletion wird aufgerufen.
1. *dounce* &rarr; Der Cursor bewegt sich einen runter auf den Eintrag "turnLeft()"
1. *shock* &rarr; Wählt den Eintrag "turnLeft()" aus.

### VII: Ein Schritt vor ###
```java 
    move();  
    move();  
    move();    
    turnLeft();  
    move();
```
1. *laws move* &rarr; Schreibt "move".
1. *call function shock* &rarr; Schreibt "();" und eine neue Zeile.




### VIII: Zeitung aufheben ###
```java 
    move();  
    move();  
    move();   
    turnLeft();  
    move();  
    pickBeeper();  
```

1. *laws pick* &rarr; Schreibt "pick".
1. *sing beeper* &rarr; Schreibt "Beeper".
1. *call function shock* &rarr; Schreibt "();" und eine neue Zeile.

### IX: Ein Schritt vor ###
```java 
move();  
    move();  
    move();  
    turnLeft();  
    move();  
    pickBeeper();  
    move();  
```

1. *laws move* &rarr; Schreibt "move".
1. *call function shock* &rarr; Schreibt "();" und eine neue Zeile.




### X: Zeitung ablegen ###
```java
    move();  
    move();  
    move();  
    turnLeft();  
    move();  
    pickBeeper();  
    move();  
    putBeeper();    
```

1. *laws put* &rarr; Schreibt "put".
1. *sing beeper* &rarr; Schreibt "Beeper".
1. *call function shock* &rarr; Schreibt "();" und eine neue Zeile.


### XI: Ein Schritt vor ###
```java
    move();  
    move();  
    move();  
    turnLeft();  
    move();  
    pickBeeper();  
    move();  
    putBeeper();  
    move();   
```
1. *laws move* &rarr; Schreibt "move".
1. *call function shock* &rarr; Schreibt "();" und eine neue Zeile.


### XII: Programm ausführen ###
1. *save* &rarr; Speichert die Datei.
1. *run karel* &rarr; Öffnet die Klassenauswahl.
1. *dunce two* &rarr; Die Klasse Basics wird ausgewählt.
1. *shock* &rarr; Das Programm wird gestartet; ein Fenster öffnet sich.
1. *tabby* &rarr; Wählt in dem Button "Start Program" aus.
1. *shock* &rarr; Drückt auf den Button.


## Teil 2: Die Zweite Zeitung aufheben ###

### I: Fenster schließen ###
1. *close this window* &rarr; Das Karel-Fenster schließen.


### II: Die ersten Moves löschen ###
```java 
    public void run() {  
        turnLeft();  
        move();  
        pickBeeper();  
        move();  
        putBeeper();  
        move();  
    }
```

1. *sauce 9* &rarr; Der Cursor beweg sich vor das erste "move();"
1. *delete line* &rarr; Löscht das erste Move.
1. *delete line* &rarr; Löscht das zweite Move.
1. *delete line* &rarr; Löscht das dritte Move.


### III: For-Schleife einfügen
  
```java 
    public void run() {  
        for(int i=0; i<5; i++) {  
            move();  
        }  
        turnLeft();  
        move();  
        pickBeeper();  
        move();  
        putBeeper();  
        move();  
    }  
```
1. *for loop* &rarr; Eine For-Schleife wird eingefügt; der Cursor steht in der Schleifenbedingung.
1. *num five* &rarr; Schreibt "5".
1. *dunce* &rarr; Der Cursor geht eine Zeile tiefer. 
1. *laws move* &rarr; Schreibt "move".
1. *call function* &rarr; Schreibt "();", **ohne** neue Zeile.



### IV: Programm erneut starten ###
1. *save* &rarr; Speichert die Datei.
1. *run last karel* &rarr; Startet das Programm.
1. *tabby* &rarr; Wählt in dem Button "Start Program" aus.
1. *shock* &rarr; Drückt auf den Button.


## Fülle nun die erste Seite des Fragebogens aus

https://docs.google.com/forms/d/e/1FAIpQLSdFterRGgbLz5jBds68-nk1w4Y596cziqQ7MrTy8kc3p5lCVw/viewform