# Einführung in die Bedienung #

Vorweg:

1. Durch *stop listening* wird die Spracherkennung pausiert, durch *start listening* wieder fortgesetzt.
2. Eine Bildschirmaufnahme starten.


## Teil 1: Diktieren ##


### I: laws, sing ###

Im Nachfolgenden werden nur einzelne Wörter benutzt, um diesen Test einfach zu halten.
Mit den Befehlen *laws* und *sing* kann unterschieden werden, ob ein Wort klein- oder großgeschrieben werden soll.

Sprich: 
 
1. *laws hello* &rarr; Es wird "hello" geschrieben, klein.  
2. *sing hello* &rarr; Es wird "Hello" geschrieben, groß.

### II: shock, ace, period ###

Durch das Wort *shock* wird die Enter-Taste betätigt, *ace* schreibt ein Leerzeichen, *period* einen Punkt.

Sprich:

1. *shock* &rarr; Eine neue Zeile.  
1. *sing hello* &rarr; Schreibt "Hello".  
2. *ace* &rarr; Schreibt ein Leerzeichen.  
3. *laws world* &rarr; "Schreibt "world".  
4. *period* &rarr; Schreibt einen Punkt.

### III: Aufgabe ###
Aufgabe: Schreibe ein paar Wörter, Leerzeichen und Punkte in die nächsten drei Zeilen.


## Teil2: Navigieren und Korrigieren ##

### I: Navigation mit lease, ross, sauce und dunce ###

Setze den Cursor manuell in die mittlere Zeile und sprich:

1. *lease* &rarr; Cursor geht einen Schritt nach links.
2. *ross* &rarr; Cursor geht einen Schritt nach rechts.
3. *sauce* &rarr; Cursor geht eine Zeile hoch.
4. *dunce* &rarr; Cursor geht eine Zeile runter.
1. *lease five* &rarr; Cursor geht zwei Schritte nach links.
2. *ross three* &rarr; Cursor geht einen Schritt nach rechts.
3. *sauce two* &rarr; Cursor geht eine Zeile hoch.
4. *dunce two* &rarr; Cursor geht eine Zeile runter.


### II: Nutzung von wally ###
Geh mit dem Cursor in die Mitte einer Zeile.

1. *lease wally* &rarr; Cursor springt an den Anfang der Zeile.
2. *ross wally*  &rarr; Cursor springt an das Ende der Zeile.


### III: clear und undo ###

1. *ross wally* &rarr; Cursor geht ans Ende der aktuellen Zeile.
2. *clear* &rarr; Das letzte Zeichen der letzten Zeile wird gelöscht.
3. *laws morning* &rarr; Schreibt "morning".
6. *undo* &rarr; "morning" wird wieder gelöscht.
7. *redo* &rarr; "morning" wird wieder geschrieben.

## Teil 3: Ausprobieren ###

Teste alle Befehle aus, die du hier gelernt hast:

1. *laws*
2. *sing*
3. *shock*
4. *ace*
5. *period*
6. *lease*
7. *ross*
8. *sauce*
9. *dunce*
10. *wally*
12. *clear*
14. *undo*
